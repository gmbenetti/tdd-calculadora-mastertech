package br.com.calculadora;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class CalculadoraTeste {

    private Calculadora calculadora;

    @BeforeEach
    public void setUp(){
        calculadora = new Calculadora();
    }

    @Test
    public void testaASomaDeDoisNumerosInteiros(){
        int resultado = calculadora.soma(1, 2);

        Assertions.assertEquals(3, resultado);
    }

    @Test
    public void testaASomaDeDoisNumerosFlutuantes(){
        double resultado = calculadora.soma(2.3, 3.4);

        Assertions.assertEquals(5.7, resultado);
    }

    @Test
    public void testaAMultiplicacaoDeDoisInteiros(){
        int resultado = calculadora.multiplicacao(3,5);

        Assertions.assertEquals(15, resultado);
    }

    @Test
    public void testaAMultiplicacaoDeDoisNegativos(){
        int resultado = calculadora.multiplicacao(-3,-10);

        Assertions.assertEquals(30,resultado);
    }

    @Test
    public void testaAMultiplicacaoDeUmNegativosEUmPositivo(){
        int resultado = calculadora.multiplicacao(-3,7);

        Assertions.assertEquals(-21,resultado);
    }

    @Test
    public void testaADivisaoDeDoisNumerosInteiros(){
        Double resultado = calculadora.divisao(10,2);

        Assertions.assertEquals(5,resultado);
    }

    @Test
    public void testaADivisaoDeDoisNumerosInteirosComDividendoMenor(){
        Double resultado = calculadora.divisao(2,10);

        Assertions.assertEquals(0.20,resultado);
    }

    @Test
    public void testaADivisaoDeDoisFlutuantes(){
        Double resultado = calculadora.divisao(3.3,1.1);

        Assertions.assertEquals(3,resultado);
    }

    @Test
    public void testaADivisaoDeDoisFlutuantesIguais(){
        Double resultado = calculadora.divisao(2.5,2.5);

        Assertions.assertEquals(1,resultado);
    }

    @Test
    public void testaADivisaoDeDoisNumerosNegativosInteiros(){
        Double resultado = calculadora.divisao(-10,-2);

        Assertions.assertEquals(5,resultado);
    }

    @Test
    public void testaAdivisaoComUmNegativo(){
        Double resultado = calculadora.divisao(-10, 2);

        Assertions.assertEquals(-5,resultado);
    }

    @Test
    public void testaADivisaoDeUmInteiroPorUmFlutuante(){
        Double resultado = calculadora.divisao(10, 2.5);

        Assertions.assertEquals(4, resultado);
    }

    @Test
    public void testaADivisaodeUmFluttuantePorUmInteiro(){
        Double resultado = calculadora.divisao(2.5, 2);

        Assertions.assertEquals(1.25,resultado);
    }
}

