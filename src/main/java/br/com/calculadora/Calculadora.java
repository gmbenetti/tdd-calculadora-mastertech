package br.com.calculadora;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.function.DoubleBinaryOperator;

public class Calculadora {

    public Calculadora() {
    }

    public int soma(int primeiroNumero, int segundoNumero) {
        int resultado;

        resultado = primeiroNumero + segundoNumero;
        return resultado;
    }

    public double soma(double primeiroNumero, double segundoNumero) {
        double resultado;
        resultado = primeiroNumero + segundoNumero;

        BigDecimal bg = new BigDecimal(resultado).setScale(3, RoundingMode.HALF_EVEN);
        resultado = primeiroNumero + segundoNumero;
        return bg.doubleValue();
    }

    public int multiplicacao(int primeiroNumero, int segundoNumero) {
        int resultado;

        resultado = primeiroNumero * segundoNumero;
        return resultado;
    }

    public Double divisao(int primeiroNumero, int segundoNumero) {
        Double resultado = Double.valueOf(primeiroNumero) / Double.valueOf(segundoNumero);

        return resultado.doubleValue();
    }

    public Double divisao(Double primeiroNumero, Double segundoNumero) {
        Double resultado = primeiroNumero / segundoNumero;
        BigDecimal bg = new BigDecimal(resultado).setScale(3, RoundingMode.HALF_EVEN);

        return bg.doubleValue();
    }

    public Double divisao(int primeiroNumero, Double segundoNumero) {
        Double resultado = Double.valueOf(primeiroNumero) / Double.valueOf(segundoNumero);
        BigDecimal bg = new BigDecimal(resultado).setScale(3, RoundingMode.HALF_EVEN);

        return bg.doubleValue();
    }

    public Double divisao(Double primeiroNumero, int segundoNumero) {
        Double resultado = Double.valueOf(primeiroNumero) / Double.valueOf(segundoNumero);
        BigDecimal bg = new BigDecimal(resultado).setScale(3, RoundingMode.HALF_EVEN);

        return bg.doubleValue();
    }
}
